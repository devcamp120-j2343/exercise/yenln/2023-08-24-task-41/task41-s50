var gBASE_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/";
var gShoppingBag = localStorage.getItem('shoppingBag') ? JSON.parse(localStorage.getItem('shoppingBag')) : [];
localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));

var gPizzaOrderList = [];

$(document).ready(function () {
    callAPIAndloadPizzaProductsIntoFE();
    callAPIAndloadBlogIntoFE();
    $('.list-pizza-kitchen-section').on('click', '.add-a-product', function () {
        addAProductIntoShoppingBag(this);
    })
    changeBagToYellow();
})

function callAPIAndloadPizzaProductsIntoFE() {
    var vAPI_URL = gBASE_URL + "/pizza";
    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        async: false,
        success: function (res) {
            console.log(res);
            loadPizzaProducts(res);
            gPizzaOrderList = res;
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function loadPizzaProducts(pizzaList) {
    $(".list-pizza-kitchen-section").html("");
    var vProduct = "";
    for (var i in pizzaList) {
        vProduct += `
        <div class="product-detail-kitchen">
        <div class="img-of-pizza-detail">
            <img alt="" src="${pizzaList[i].imageUrl}">
        </div>
        <div class="information-each-pizza-detail">
            <div>
                <span class="title-pizza-kitchen">${pizzaList[i].name}</span>
                <span class="title-pizza-kitchen">$${pizzaList[i].price}</span>
            </div>
            <div>
                <div>
                    <span class="has-border-detail"><i class="fa-solid fa-star"></i>&nbsp;${pizzaList[i].rating}</span>
                    <span class="has-border-detail">${pizzaList[i].time}</span>
                </div>
                <button data-id=${pizzaList[i].id} class="add-a-product">
                    <span class="choosing-add-outside"><i class="fa-solid fa-plus"></i></span>
                </button>
            </div>
        </div>
    </div>

        `
    }
    $(".list-pizza-kitchen-section").html(vProduct);

}

function callAPIAndloadBlogIntoFE() {
    var vAPI_URL = gBASE_URL + "/blogs";
    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        success: function (res) {
            console.log(res);
            loadBlog(res);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function loadBlog(listBlog) {
    $(".blog-card-left-list").html("");
    var vBlogList = "";
    for (let i in listBlog) {
        if (i < 2) {
            vBlogList += `
            <div class="blog-card-left-1">
                <div >
                    <picture class="blog-card-picture"><img src="${listBlog[i].imageUrl}"></picture>
                </div>
                <div class="blog-card-information">
                    <h5>${listBlog[i].title} </h5>
                    <span class="blog-p">
                        <p>${listBlog[i].description}</p>
                    </span>
                </div>
            </div>
`
        }
    }
    $(".blog-card-left-list").html(vBlogList);

    $(".blog-card-center").html("");
    var vBlogList = "";
    for (let i in listBlog) {
        if (i == 2) {
            vBlogList += `
            <picture class="middle-pic-blog-section"><img alt="" src="${listBlog[i].imageUrl}"></picture>
            <div class="blog-card-information">
                <h5>${listBlog[i].title}</h5>
                <span class="blog-p">
                    <p>${listBlog[i].description}</p>
                </span>
            </div>
`
        }
    }
    $(".blog-card-center").html(vBlogList);

    $(".blog-card-right-list").html("");
    var vBlogList = "";
    for (let i in listBlog) {
        if (i > 2) {
            vBlogList += `
            <div class="blog-card-right-1">
            <div >
                <picture class="blog-card-picture"><img src="${listBlog[i].imageUrl}"></picture>
            </div>
            <div class="blog-card-information">
                <h5>${listBlog[i].title} </h5>
                <span class="blog-p">
                    <p>${listBlog[i].description}</p>
                </span>
            </div>
        </div>
`
        }
    }
    $(".blog-card-right-list").html(vBlogList);


}

function addAProductIntoShoppingBag(element) {
    var vPizzaId = $(element).data("id");
    for (let i in gPizzaOrderList) {
        if (vPizzaId == gPizzaOrderList[i].id) {
            gShoppingBag.push(gPizzaOrderList[i]);
            localStorage.setItem('shoppingBag', JSON.stringify(gShoppingBag));
        }
    }
    window.location.href = window.location.href;
}

function changeBagToYellow() {
    if (gShoppingBag.length > 0) {
        $("#shopping-bag-icon").attr('src',`images/yellow-shopping-bag.png`)
    }
    else {
        $("#shopping-bag-icon").attr('src',`images/akar-icons_shopping-bag-white.png`);
    }
}